package com.sparktutorial.spark;

import com.google.common.io.CharStreams;
import com.google.common.io.Resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Properties;

public class OptionParser {
    private final URL defaultPropertiesResource;

    public OptionParser()
    {
        this.defaultPropertiesResource = null;
    }

    public OptionParser(String resource)
    {
        this(Resources.getResource(resource));
    }

    public OptionParser(URL defaultPropertiesResource)
    {
        this.defaultPropertiesResource = defaultPropertiesResource;
    }

    public Properties parse(String[] args) {
        LinkedList<String> argq = new LinkedList<String>();
        argq.addAll(Arrays.asList(args));

        Properties properties = new Properties();

        if(defaultPropertiesResource != null)
            try {
                properties.load(defaultPropertiesResource.openStream());
            }catch (IOException e)
            {
                System.err.println(String.format("Failed to load default properties from %s: %s", defaultPropertiesResource, e.getMessage()));
                e.printStackTrace();
            }


        while(!argq.isEmpty())
        {
            String arg = argq.removeFirst().trim();
            if(arg.equals("-defaults"))
            {
                if(defaultPropertiesResource != null)
                    try {
                        System.err.println(CharStreams.toString(new InputStreamReader(defaultPropertiesResource.openStream())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                else
                    System.err.println("<No default properties>");
                System.exit(0);
                return null;
            }
            else
            if(arg.equals("-h"))
            {
                System.err.println(usage());
                System.exit(0);
                return null;
            }
            else
            if(arg.equals("-P"))
            {
                if(argq.isEmpty())
                {
                    System.err.println("Must provide a valid properties file with -P option. No file provided");
                    System.exit(1);
                    return null;
                }
                String filename = argq.removeFirst();
                try {
                    properties.load(new FileInputStream(filename));
                } catch (IOException e) {
                    System.err.println(String.format("Error loading file %s: %s", filename, e.getMessage()));
                    System.exit(1);
                }
            }
            else
            if(arg.startsWith("-D"))
            {
                String value = arg.substring(2);
                if(value.length() <= 0)
                try{value = argq.removeFirst();}catch(NoSuchElementException e)
                {
                    System.err.println("Must provide a valid parameter value. E.g. -Dparam1=value1 or -D param1=value1.");
                    System.exit(1);
                    return null;
                }

                String[] keyValue = value.split("=");
                properties.setProperty(keyValue[0].trim(), keyValue[1].trim());
            }
            else
            {
                System.err.println("Unrecognized option: " + arg);
                System.err.println(usage());
                System.exit(1);
                return null;
            }

        }
        return properties;
    }

    public String usage(){
        return "Usage: <command> [-P file.properties|-h|-Dparam=value|-defaults]";
    }
}